/*
    Purpose: 
    Author: Reece W. 
    License: All Rights Reserved J. Reece Wilson
*/  

#include <xenus_subsystem.hpp>
#include "SelfTestAll.hpp"

void selftest_memleak()
{
	PRINT_WARN("Testing for memory leaks. kernel may panic");
	for (int i = 0; i < 30000; i++)
		free(malloc(1024 * 1024 * 4)); // Linux kernel should panic on failure
	PRINT_PASS("Memory leak test");
}
/*
    Purpose:
    Author: Reece W. 
    License: All Rights Reserved J. Reece Wilson
*/  

#include <xenus_subsystem.hpp>
#include "SelfTestAll.hpp"
#include "TestFPU.hpp"

DEFINE_SYSV_FUNCTON_START(sysx_test, size_t)
uint64_t a_1,
uint64_t a_2,
uint64_t a_3,
uint64_t a_4,
uint64_t a_5,
uint64_t a_6,
uint64_t a_7,
uint64_t a_8,
uint64_t a_9,
uint64_t a_10,
uint64_t a_11,
uint64_t a_12,
uint64_t a_13,
uint64_t a_14,
uint64_t a_15,
DEFINE_SYSV_FUNCTON_END_DEF(sysx_test, size_t)
{
	test_fpu("SystemV callback stub");

#define CHK_ARG(n)\
	if (a_##n != n) PRINT_ERROR("SystemV bad argument in callback");

	CHK_ARG(1)
	CHK_ARG(2)
	CHK_ARG(3)
	CHK_ARG(4)
	CHK_ARG(5)
	CHK_ARG(6)
	CHK_ARG(7)
	CHK_ARG(8)
	CHK_ARG(9)
	CHK_ARG(10)
	CHK_ARG(11)
	CHK_ARG(12)
	CHK_ARG(13)
	CHK_ARG(14)
	CHK_ARG(15)

	PRINT_PASS("Callback stub test okay");
	return 1;
}
DEFINE_SYSV_FUNCTON_END


void selftest_callbacks()
{
	void * sysv;
	void * handle;

	PRINT_WARN("Testing dynamic callbacks");

	if (ERROR(dyncb_allocate_stub(not_callable_sysx_test, 15, nullptr, &sysv, &handle)))
	{
		PRINT_ERROR("Dynamic callback stub creation failed");
		return;
	}
	test_function(69, (size_t)sysv, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	if (ERROR(dyncb_free_stub(handle)))
	{
		PRINT_ERROR("Dynamic callback stub clean up failed");
		return;
	}

}
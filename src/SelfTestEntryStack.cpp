/*
    Purpose: 
    Author: Reece W. 
    License: All Rights Reserved J. Reece Wilson
*/  

#include <xenus_subsystem.hpp>
#include "SelfTestAll.hpp"
#include "TestFPU.hpp"

void selftest_stack()
{
	chkstack_t n;
	test_fpu("MSVC generic floating point & stack alignment test");

	if (((size_t)(&n) % 16) != 0)
		PRINT_ERROR("Stack alignment");
	else
		PRINT_OKAY("Stack alignment");
}
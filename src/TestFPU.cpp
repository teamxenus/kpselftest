/*
    Purpose: 
    Author: Reece W. 
    License: All Rights Reserved J. Reece Wilson
*/  

#include <xenus_subsystem.hpp>
#include "SelfTestAll.hpp"
#include "TestFPU.hpp"

void test_fpu(const char * str)
{
	PRINT_WARN("Testing reason: %s. A crash may occur if the FPU isn't enabled.", str);
	PRINT_OKAY("%H (0dp = 420), %H:2 (2dp = 420.30), %H:3 (3dp = 420.300)", double(420.20 + 0.10), double(420.20 + 0.10), double(420.20 + 0.10), double(420.20 + 0.10), double(420.20 + 0.10));
}
/*
    Purpose:
    Author: Reece W. 
    License: All Rights Reserved J. Reece Wilson
*/  

#include <xenus_subsystem.hpp>
#include "SelfTestAll.hpp"

void selftest_unicode()
{
	printf("\r[FAIL] Unicode test failed %S\n", L"\r[PASS] Pseudo-unicode test passed. ASCII translation is fully-ish functional");
}
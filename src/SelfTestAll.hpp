#define PRINT_ERROR(fmt, ...) printf("\r[FAIL] " fmt "\n", __VA_ARGS__)
#define PRINT_PASS(fmt, ...)  printf("\r[PASS] " fmt "\n", __VA_ARGS__)
#define PRINT_OKAY(fmt, ...)  printf("\r[OKAY] " fmt "\n", __VA_ARGS__)
#define PRINT_WARN(fmt, ...)  printf("\r[WARN] " fmt "\n", __VA_ARGS__)
#define assert(tru, fmt, ...) if (!(tru)) PRINT_ERROR(fmt, __VA_ARGS__);

void selftest_unicode();
void selftest_memleak();
void selftest_callbacks();
void selftest_stack();


static void selftest_all()
{
	selftest_unicode();
	selftest_memleak();
	selftest_callbacks();
	selftest_stack();
}
﻿/*
    Purpose: Self test plugin.
    Author: Reece W. 
    License: All Rights Reserved J. Reece Wilson
*/  

#include <xenus_subsystem.hpp>
#include "SelfTestAll.hpp"

mod_global_data_t module;

int plugin_selftest_start()
{
	selftest_all();
	return 1;
}

bool plugin_selftest_init(mod_dependency_list_p deps)
{
	return true;
}

void plugin_selftest_shutdown()
{
	
}

void entrypoint(xenus_entrypoint_ctx_p context)
{
	context->size					= sizeof(xenus_entrypoint_ctx_t);
    context->description			= "Xenus LibOS and Kernel checks.";
    context->copyright				= "All rights reserved, Reece Wilson (2018)";
    context->init					= plugin_selftest_init;
    context->start					= plugin_selftest_start;
    context->shutdown				= plugin_selftest_shutdown;
	context->dependencies.count		= 0;
	context->static_data			= &module;
}